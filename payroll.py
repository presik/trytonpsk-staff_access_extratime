import datetime

from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval


class Payroll(metaclass=PoolMeta):
    __name__ = "staff.payroll"
    start_extras = fields.Date('Start Extras', states={
            'readonly': Eval('state') != 'draft',
        })
    end_extras = fields.Date('End Extras', states={
            'readonly': Eval('state') != 'draft',
        })
    assistance = fields.One2Many('staff.access', 'payroll', 'Assistance',
        states={'readonly': True})

    def set_preliquidation(self, config, extras, discounts=None,
        cache_wage_dict=None):
        pool = Pool()
        _extras = {}
        if self.employee.position and self.employee.position.extras:
            Access = pool.get('staff.access')
            # config = pool.get('staff.configuration')(1)
            if self.start_extras and self.end_extras:
                start_date = self.start_extras
                end_date = self.end_extras
            else:
                start_date = self.start
                end_date = self.end
            start = datetime.datetime.combine(start_date, datetime.time(0, 0, 0))
            end = datetime.datetime.combine(end_date, datetime.time(23, 59, 59))

            accesses = Access.search([
                ('employee', '=', self.employee.id),
                ('enter_timestamp', '>=', start),
                ('enter_timestamp', '<=', end),
                ('state', '=', 'close'),
            ])
            shifts_to_pay = {}
            extra_payments = {}
            extra_shift = {}
            if accesses:
                extras = {
                    'hedo': [],
                    'heno': [],
                    'hedf': [],
                    'henf': [],
                    'hedoa': [],
                    'henoa': [],
                    'hedfa': [],
                    'henfa': [],
                    'reco': [],
                    'recf': [],
                    'dom': [],
                }
                # limit_shift_month = config.limit_shift_month
                # wage_shift_fixed = config.wage_shift_fixed
                # shift_limit = hasattr(config, 'shift_limit')
                exist_extra_payments = False
                if len(accesses) > 0 and hasattr(accesses[0], 'extra_payments'):
                    exist_extra_payments = True
                for acc in accesses:
                    if hasattr(acc, 'extra_shift') and acc.extra_shift:
                        key = str(acc.wage_type_extra_shift.id) + '_' + str(acc.shift_amount)
                        if key not in extra_shift:
                            extra_shift[key] = {
                                'amount': acc.shift_amount,
                                'wage': cache_wage_dict[acc.wage_type_extra_shift.id],
                                'qty': [1],
                            }
                        else:
                            extra_shift[key]['qty'].append(1)

                    elif acc.payment_method == 'fixed_amount':
                        if acc.shift_amount not in shifts_to_pay:
                            shifts_to_pay[acc.shift_amount] = []
                        shifts_to_pay[acc.shift_amount].append(1)
                    else:
                        for ext in extras:
                            if hasattr(acc, ext):
                                if val := getattr(acc, ext):
                                    extras[ext].append(val)
                    if exist_extra_payments:
                        for e in acc.extra_payments:
                            key = str(e.wage_type.id) + '_' + str(e.amount)
                            if key not in extra_payments:
                                extra_payments[key] = {
                                    'amount':  e.amount,
                                    'wage': cache_wage_dict[e.wage_type.id],
                                    'qty': [1],
                                }
                            else:
                                extra_payments[key]['qty'].append(1)

                for ek in extras:
                    sum_extra = sum(extras[ek])
                    if sum_extra > 0:
                        _extras[ek] = sum_extra

                Access.write(accesses, {'state': 'done', 'payroll': self.id})

            if hasattr(config, 'wage_shift_fixed') and \
                    config.wage_shift_fixed and shifts_to_pay:
                _extras['wage_shift_fixed'] = []
                for amount, qty in shifts_to_pay.items():
                    _extras['wage_shift_fixed'].append({
                        'wage': cache_wage_dict[config.wage_shift_fixed.id],
                        'qty': sum(qty),
                        'unit_value': amount,
                    })
            if extra_payments:
                for v in extra_payments.values():
                    _extras['wage_shift_fixed'].append({
                        'wage': v['wage'],
                        'qty': sum(v['qty']),
                        'unit_value': v['amount'],
                    })
            if extra_shift:
                for v in extra_shift.values():
                    _extras['wage_shift_fixed'].append({
                        'wage': v['wage'],
                        'qty': sum(v['qty']),
                        'unit_value': v['amount'],
                    })
        super(Payroll, self).set_preliquidation(config, _extras, discounts, cache_wage_dict)

    @classmethod
    def delete(cls, records):
        Access = Pool().get('staff.access')
        # Cancel before delete
        cls.cancel(records)
        payroll_ids = [p.id for p in records]
        accesses = Access.search([
            ('payroll', 'in', payroll_ids),
        ])
        for acc in accesses:
            Access.write([acc], {'state': 'close', 'payroll': None})
        super(Payroll, cls).delete(records)

    @classmethod
    def draft(cls, records):
        super(Payroll, cls).draft(records)
        Access = Pool().get('staff.access')
        for payroll in records:
            accesses = Access.search([
                ('payroll', '=', payroll.id),
            ])
            for acc in accesses:
                Access.write([acc], {'state': 'close'})

    @classmethod
    def process(cls, records):
        super(Payroll, cls).process(records)
        Access = Pool().get('staff.access')
        for payroll in records:
            accesses = Access.search([
                ('payroll', '=', payroll.id),
            ])
            for acc in accesses:
                Access.write([acc], {'state': 'done'})


class PayrollGroupStart(metaclass=PoolMeta):
    __name__ = 'staff.payroll_group.start'
    start_extras = fields.Date('Start Extras')
    end_extras = fields.Date('End Extras')


class PayrollGroup(metaclass=PoolMeta):
    __name__ = 'staff.payroll_group'

    def get_values(self, employee, start_date, end_date):
        res = super(PayrollGroup, self).get_values(employee, start_date, end_date)
        if self.start.start_extras and self.start.end_extras:
            res['start_extras'] = self.start.start_extras
            res['end_extras'] = self.start.end_extras
        return res
