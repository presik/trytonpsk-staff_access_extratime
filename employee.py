import base64
import pickle

from sql import Literal, Null
from trytond.exceptions import UserError
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction

FINGERS = [
    ('left_thumb', 'Left thumb'),
    ('left_index',  'Left index finger'),
    ('left_middle',  'Left middle finger'),
    ('left_ring',  'Left ring finger'),
    ('left_little',  'Left little finger'),
    ('right_little',  'Right little finger'),
    ('right_thumb',  'Right thumb finger'),
    ('right_middle',  'Right middle finger'),
    ('right_ring',  'Right ring finger'),
    ('right_index',  'Right index finger'),
    ('', ''),
]

TYPE_ACCESS = [
    ('finger_access', 'Finger Access'),
    ('face_access',  'Face Access'),
    ('', ''),
]

STATE_FINGER = {
    'invisible': Eval('access_type') != 'finger_access',
}

class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    access_type = fields.Selection(TYPE_ACCESS, 'Access Type')
    password_access = fields.Char('Password')
    byte_finger = fields.Binary('Finger Enrolled', states=STATE_FINGER)
    enroll_date = fields.Date('Enroll Date', states=STATE_FINGER)
    finger = fields.Selection(FINGERS, 'Finger', states=STATE_FINGER)
    external = fields.Boolean('External',
        'Mark this option when employee is external', states=STATE_FINGER)

    @classmethod
    def validate(cls, records):
        non_empty_records = [r for r in records if r.password_access and r.password_access != '']
        for record in non_empty_records:
            existing_record = cls.search([
                ('password_access', '=', record.password_access),
                ('id', '!=', record.id)])
            if existing_record:
                raise UserError('La contraseña ya está ocupada por otro empleado')
        super(Employee, cls).validate(records)

    @classmethod
    def __register__(cls, module_name):
        table = cls.__table_handler__(module_name)
        sql_table = cls.__table__()

        if table.column_exist("password_access"):
            cursor = Transaction().connection.cursor()
            cursor.execute(
                *sql_table.update(
                    columns=[sql_table.password_access],
                    values=[Null],
                    where=(sql_table.password_access == Literal(''))
                )
            )
        table.drop_constraint('password_access_uniq')
        super().__register__(module_name)
