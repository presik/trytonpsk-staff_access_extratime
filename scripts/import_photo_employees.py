import os
import pickle
import sys
from pathlib import Path

sys.path.append(str(Path(__file__).parent.parent))
from argparse import ArgumentParser

from lib.face_recognition import FaceRecognition

try:
    from proteus import Model, config
except ImportError:
    prog = os.path.basename(sys.argv[0])
    sys.exit("proteus must be installed to use %s" % prog)

IMAGES_FOLDER = "photo_employees"

def image_to_binary(file_path):
    with open(file_path, 'rb') as file:
        binary_data = file.read()
    return binary_data

def generate_binary_embedding(binary):
    fr = FaceRecognition()
    embedded = fr.prepare_image_to_save(binary)
    binary_embedded = pickle.dumps(embedded)
    return binary_embedded

def do_import_photo(home=str(Path.home())):
    photo_employees_folder = os.listdir(f"{home}/{IMAGES_FOLDER}/")
    Employee = Model.get("company.employee")
    if photo_employees_folder:
        for file in photo_employees_folder:
            try:
                file_path = os.path.join(home, IMAGES_FOLDER, file)
                party_number = file.split(".")[0]
                employee, = Employee.find([
                    ("party.id_number", "=", party_number)
                ])
                binary_image = image_to_binary(file_path)
                binary_embedded = generate_binary_embedding(binary_image)
                if employee and binary_image is not None and binary_embedded is not None:
                    employee.access_type = "face_access"
                    employee.face_photo = binary_image
                    employee.face_print = binary_embedded
                    employee.save()
            except (ValueError, TypeError) as e:
                raise Exception(f'Error en el proceso: {str(e)}')

def main(database, config_file=None):
    config.set_trytond(database, config_file=config_file)
    with config.get_config().set_context(active_test=False):
        do_import_photo()

def run():
    parser = ArgumentParser()
    parser.add_argument('-d', '--database', dest='database', required=True)
    parser.add_argument('-c', '--config', dest='config_file',
        help='the trytond config file')

    args = parser.parse_args()
    main(args.database, args.config_file)

if __name__ == '__main__':
    run()
