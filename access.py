# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import contextlib
from datetime import datetime, time, timedelta
from decimal import Decimal

from dateutil import tz
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.model import ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateTransition, StateView, Wizard

from .exceptions import AccessDeleteError

from_zone = tz.gettz('UTC')
to_zone = tz.gettz('America/Bogota')

# Convertir en un campo para parametrizar desde configuracion
# WORKDAY_DEFAULT = 7.6669
RESTDAY_DEFAULT = 0

WEEK_DAYS = {
    1: 'monday',
    2: 'tuesday',
    3: 'wednesday',
    4: 'thursday',
    5: 'friday',
    6: 'saturday',
    7: 'sunday',
}

_ZERO = Decimal('0.0')
STATES = {'readonly': (Eval('state') == 'done')}
PAYMENT_METHOD = [
    ('extratime', 'Extratime'),
    ('fixed_amount', 'Fixed Amount'),
]

REQUEST_STATE = [
    ('in_process', 'In Process'),
    ('approved', 'Approved'),
    ('rejected', 'Rejected'),
    ('', ''),
]


def round_value(n):
    return Decimal(str(round(n, 2)))


class StaffAccess(metaclass=PoolMeta):
    __name__ = 'staff.access'
    ttt = fields.Numeric('TTT', digits=(3, 2))
    het = fields.Numeric('HET', digits=(3, 2))
    hedo = fields.Numeric('HEDO', digits=(3, 2))
    heno = fields.Numeric('HENO', digits=(3, 2))
    reco = fields.Numeric('RECO', digits=(3, 2))
    recf = fields.Numeric('RECF', digits=(3, 2))
    dom = fields.Numeric('DOM', digits=(3, 2))
    hedf = fields.Numeric('HEDF', digits=(3, 2))
    henf = fields.Numeric('HENF', digits=(3, 2))
    cost_het = fields.Function(fields.Numeric('Cost HET',
        digits=(16, 2)), 'get_cost_het')
    cost_hedo = fields.Function(fields.Numeric('Cost HEDO',
        digits=(16, 2)), 'get_cost_extra')
    cost_heno = fields.Function(fields.Numeric('Cost HENO',
        digits=(16, 2)), 'get_cost_extra')
    cost_reco = fields.Function(fields.Numeric('Cost RECO',
        digits=(16, 2)), 'get_cost_extra')
    cost_recf = fields.Function(fields.Numeric('Cost RECF',
        digits=(16, 2)), 'get_cost_extra')
    cost_dom = fields.Function(fields.Numeric('Cost DOM',
        digits=(16, 2)), 'get_cost_extra')
    cost_hedf = fields.Function(fields.Numeric('Cost HEDF',
        digits=(16, 2)), 'get_cost_extra')
    cost_henf = fields.Function(fields.Numeric('Cost HENF',
        digits=(16, 2)), 'get_cost_extra')
    # notes = fields.Text('Notes')
    # project = fields.Many2One('project.work', 'Project')
    payroll = fields.Many2One('staff.payroll', 'Payroll', ondelete='RESTRICT',
        readonly=True)
    shift_amount = fields.Numeric('Shift Amount')
    shift_amount_add = fields.Numeric('Shift Amount Additional')
    payment_method = fields.Selection(PAYMENT_METHOD, 'Payment Method',
        required=True)
    request_state = fields.Selection(REQUEST_STATE, 'Request State', readonly=True)
    requested_by = fields.Many2One('company.employee', 'Requested By',
        readonly=True)
    request_concept = fields.Text('Request Concept', states={
        'readonly': Eval('request_state').in_(['in_process', 'approved']),
    })
    approval_notes = fields.Text('Approval Notes', states={
        'readonly': Eval('request_state').in_(['rejected', 'approved']),
    })
    approved_by = fields.Many2One('company.employee', 'Approved By', readonly=True)
    approved_date = fields.DateTime('Approved Date', readonly=True)

    @classmethod
    def __setup__(cls):
        super(StaffAccess, cls).__setup__()

        cls._buttons.update({
                'request': {
                    'invisible': Eval('request_state').in_(['in_process', 'approved']),
                    'icon': 'tryton-forward',
                    },
                'approve': {
                    'invisible': Eval('request_state').in_(['rejected', 'approved', '', None]),
                    'icon': 'tryton-forward',
                    },
                'reject': {
                    'invisible': Eval('request_state').in_(['', None, 'rejected']),
                       'icon': 'tryton-clear',
                    },
                })

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        super().__register__(module_name)
        if table_h.column_exist('notes'):
            table_h.column_rename('notes', 'request_concept')

    @staticmethod
    def default_payment_method():
        return 'extratime'

    @classmethod
    def create(cls, vlist):
        acc_ids = super(StaffAccess, cls).create(vlist)
        for val, acc_id in zip(vlist, acc_ids, strict=False):
            if val.get('enter_timestamp') and val.get('exit_timestamp'):
                cls.write([acc_id], val)
        return acc_ids

    @classmethod
    def write(cls, records, vals):
        v_keys = vals.keys()
        v_eval = ['enter_timestamp', 'exit_timestamp', 'rest', 'start_rest', 'end_rest']
        if any(item in v_keys for item in v_eval):
            vals = {
                kvalue: vals[kvalue] for kvalue in v_eval if vals.get(kvalue)
            }
            if not records[0].employee.external:
                extras = cls.get_extras(records[0], vals)
                vals.update(extras)
        super(StaffAccess, cls).write(records, vals)

    @classmethod
    def delete(cls, records):
        for acc in records:
            if acc.state == 'done' and acc.payroll:
                raise AccessDeleteError(
                    gettext('staff_access_extratime.msg_delete_cancel', acc=acc.rec_name))
        super(StaffAccess, cls).delete(records)

    @classmethod
    def set_access_control(cls, args: dict):
        """set access of employee in access control class

        Args:
            employee_id (integer): id of employee
            option_access (string): access direction employee options
            ['employee_in', 'employee_out', 'employee_in_rest', 'employee_out_rest']
        """
        Employee = Pool().get('company.employee')
        employee = Employee(args['employee_id'])
        option_access = args['option_access']
        password = args.get('password')

        dict_option_access = {
            'employee_in': 'enter_timestamp',
            'employee_out': 'exit_timestamp',
            'employee_in_rest': 'start_rest',
            'employee_out_rest': 'end_rest',
            }
        time_zone = employee.company.timezone
        time = datetime.now(tz.gettz(time_zone))
        timedelta_ = time - timedelta(hours=18)
        str_timedelta = timedelta_.strftime('%Y-%m-%d %H:%M:%S')
        attr_option_access = dict_option_access[option_access]
        value = {
            'payment_method': 'extratime',
            'state': 'close',
            'employee': employee.id,
        }
        result = {
            'status': 'success',
            'name': employee.rec_name,
            'time': time.strftime('%Y-%m-%d %I:%M %P'),
            'msg': 'REGISTRO EXITOSO',
            }

        if (password and employee.password_access and
                employee.password_access == password) or password is None:
            value[attr_option_access] = time
            access = None
            enter_delta = None
            exit_ = None
            start_rest = None
            with contextlib.suppress(Exception):
                access, = cls.search([
                    ('employee', '=', employee.id),
                    ('create_date', '>=', str_timedelta),
                ], order=[('id', 'DESC')], limit=1)
            if access:
                enter_ = access.enter_timestamp.replace(tzinfo=from_zone)
                enter_ = access.enter_timestamp.astimezone(to_zone)
                enter_delta = enter_ + timedelta(hours=18)
                enter_delta2 = enter_ + timedelta(hours=2)
                start_rest = access.start_rest
                if access.exit_timestamp:
                    exit_ = access.exit_timestamp.replace(tzinfo=from_zone)
                    exit_ = access.exit_timestamp.astimezone(to_zone)
            if option_access == 'employee_out' and access and access.exit_timestamp is None and enter_delta2 < time:
                value[attr_option_access] = value[attr_option_access].astimezone(from_zone)
                cls.write([access], value)
            elif option_access == 'employee_out' and access and not access.exit_timestamp and enter_delta2 > time:
                result['msg'] = 'Marcacion de salida no puede ser registrada ya que ingreso hace menos de dos horas'
                result['status'] = 'error'
            elif option_access == 'employee_out' and not access:
                result['msg'] = 'Marcacion de salida sin ingreso'
                result['status'] = 'error'
            elif option_access == 'employee_in' and (not access or exit_ or (enter_delta and enter_delta < time)):
                cls.create([value])
            elif option_access == 'employee_in' and access and not exit_ and enter_delta and enter_delta > time:
                result['msg'] = 'Tienes una marcacion de ingreso sin salida reciente'
                result['status'] = 'error'
            elif option_access == 'employee_out_rest' and not start_rest:
                result['msg'] = 'No tienes un registro de inicio de descanso'
                result['status'] = 'error'
            elif option_access == 'employee_in_rest' and exit_:
                result['msg'] = 'Tienes una salida ya registrada'
                result['status'] = 'error'
            elif option_access in ('employee_in_rest', 'employee_out_rest') and access:
                cls.write([access], value)
            elif option_access in ('employee_in_rest', 'employee_out_rest') and not access:
                result['msg'] = 'Marcacion de inicio/fin descanso sin ingreso reciente'
                result['status'] = 'error'
            else:
                result['status'] = 'error'
                result['msg'] = 'registro no se ha podido guardar'

            if access and not access.employee.external:
                to_write = access.get_extras(access, {})
                cls.write([access], to_write)
            return result
        else:
            result['status'] = 'error'
            result['msg'] = 'usuario sin contraseña o contraseña incorrecta'
            return result

    @classmethod
    @ModelView.button
    def request(cls, accesses):
        pool = Pool()
        User = pool.get('res.user')
        user = User(Transaction().user)

        if not user.employee:
            raise UserError(gettext('You must be asign the default employee'))

        for access in accesses:
            missing_fields = []

            if not (access.enter_timestamp and access.exit_timestamp):
                missing_fields.append('entry and exit')
            if not access.activity:
                missing_fields.append('activity')
            if not access.billable_type:
                missing_fields.append('billable type')
            if not access.request_concept:
                missing_fields.append('request concept')

            if missing_fields:
                raise UserError(
                    gettext('Check the following fields to request approval: %s')
                    % ', '.join(missing_fields)
                )

            access.requested_by = user.employee
            access.request_state = 'in_process'

        cls.save(accesses)
        pass

    @classmethod
    @ModelView.button
    def approve(cls, accesses):
        pool = Pool()
        User = pool.get('res.user')
        user = Transaction().user
        user = User(user)
        for access in accesses:
            if not user.employee:
                raise UserError('Debes Tener un empleado asignado a tu usuario')
            if access.requested_by == user.employee:
                raise UserError('No puede aprobar sus propias solicitudes')
            access.approved_by = user.employee
            access.request_state = 'approved'
            access.approved_date = datetime.now()
        cls.save(accesses)
        pass

    @classmethod
    @ModelView.button
    def reject(cls, accesses):
        pool = Pool()
        User = pool.get('res.user')
        user = Transaction().user
        user = User(user)
        for access in accesses:
            if access.requested_by == user.employee:
                raise UserError(gettext('Cannot reject your own requests'))
            if access.approved_by and access.approved_date:
                access.approved_by = user.employee
                access.approved_date = None
            access.request_state = 'rejected'
        cls.save(accesses)
        pass

    def get_cost_het(self, name):
        res = [v for v in [self.cost_hedo, self.cost_heno, self.cost_reco,
            self.cost_dom, self.cost_hedf, self.cost_henf, self.cost_recf,
            ] if v is not None]
        if res:
            return sum(res)

    def get_cost_extra(self, name):
        WageType = Pool().get('staff.wage_type')
        MandatoryWage = Pool().get('staff.payroll.mandatory_wage')
        name = name[5:]
        value_qty = getattr(self, name)
        mandatory_wages = MandatoryWage.search([
            ('wage_type.name', 'ilike', f'{name.upper()}%'),
            ('wage_type.type_concept', '=', 'extras'),
            ('employee', '=', self.employee.id),
        ], limit=1)
        wages = [m.wage_type for m in mandatory_wages]
        if not wages:
            wages = WageType.search([
                ('name', 'ilike', f'{name.upper()}%'),
                ('type_concept', '=', 'extras'),
            ], limit=1)
            if not wages:
                return

        wage = wages[0]
        if value_qty:
            args = {'salary': self.employee.salary}
            unit_value = WageType.compute_formula(wage.unit_price_formula,
                args)
            val = Decimal(round(value_qty * unit_value, 2))
            return val

    def compute_unit_value(self, salary, name):
        WageType = Pool().get('staff.wage_type')
        wage_type = WageType.search([('name', '=', name)], limit=1)
        if not wage_type or not salary:
            return _ZERO
        wage_type = wage_type[0]
        val = WageType.compute_unit_price(
            wage_type.unit_price_formula, {'salary': float(salary)},
        )
        return val

    @classmethod
    def get_extras(cls, acc, vals):
        if 'enter_timestamp' in vals:
            acc.enter_timestamp = vals.get('enter_timestamp')
        if 'exit_timestamp' in vals:
            acc.exit_timestamp = vals.get('exit_timestamp')
        if 'rest' in vals:
            acc.rest = vals.get('rest')
        if 'start_rest' in vals:
            acc.start_rest = vals.get('start_rest')
        if 'end_rest' in vals:
            acc.end_rest = vals.get('end_rest')

        # this line define workday and in the next function _get_extras() dont set workday for the position
        # workday = WORKDAY_DEFAULT
        workday = None
        # if acc.shift_kind:
        #     workday = acc.shift_kind.legal_work_time
        extras = acc._get_extras(acc.employee, acc.enter_timestamp,
            acc.exit_timestamp, acc.start_rest, acc.end_rest, acc.rest,
            workday)
        return extras

    def _get_extras(self, employee, enter_timestamp, exit_timestamp,
            start_rest, end_rest, rest, workday=None, restday=None):
        pool = Pool()
        Workday = pool.get('staff.workday_definition')
        Holiday = pool.get('staff.holidays')
        Contract = pool.get('staff.contract')
        config = Pool().get('staff.configuration')(1)

        start_date = (enter_timestamp + timedelta(hours=5)).date()
        contracts = Contract.search(['OR', [
                ('employee', '=', employee.id),
                ('start_date', '<=', start_date),
                ('finished_date', '>=', start_date),
            ], [
                ('employee', '=', employee.id),
                ('start_date', '<=', start_date),
                ('finished_date', '=', None),
            ]], limit=1, order=[('start_date', 'DESC')])

        if not contracts:
            raise UserError(
                gettext(
                    'staff_access_extratime.msg_missing_contract_for_access',
                    employee=employee.party.name,
                    date_=start_date,
                ))

        position_ = contracts[0].position
        if not enter_timestamp or not exit_timestamp \
                or not position_ or not position_.extras:
            return {
                'ttt': 0, 'het': 0, 'hedo': 0, 'heno': 0,
                'reco': 0, 'recf': 0, 'dom': 0, 'hedf': 0, 'henf': 0,
            }

        holidays = [day.holiday for day in Holiday.search([])]

        # Ajuste UTC tz para Colombia timestamp [ -5 ]
        enter_timestamp = enter_timestamp.replace(tzinfo=from_zone)
        enter_timestamp = enter_timestamp.astimezone(to_zone)

        exit_timestamp = exit_timestamp.replace(tzinfo=from_zone)
        exit_timestamp = exit_timestamp.astimezone(to_zone)

        # Contexto de cambio de turno
        weekday_number = int(enter_timestamp.strftime("%u"))
        weekday_ = WEEK_DAYS[weekday_number]
        if not workday:
            # position = employee.contract.position or employee.position
            day_work = Workday.search([
                ('weekday', '=', weekday_),
                ('position', '=', position_.id),
            ])
            if day_work and enter_timestamp.date() not in holidays:
                workday, restday = day_work[0].workday, day_work[0].restday
            else:
                workday = config.default_hour_workday
            if weekday_ == 7 and workday == 0:
                workday = config.default_hour_workday
        if not restday:
            restday = RESTDAY_DEFAULT
            print("Warning: Using default restday!")

        restday_effective = 0
        if rest:
            restday_effective = Decimal(rest)

        # Verifica si el usuario sale o entra un festivo
        enter_holiday = False
        exit_holiday = False
        if (enter_timestamp.weekday() == 6) or (enter_timestamp.date() in holidays):
            enter_holiday = True
        if (exit_timestamp.weekday() == 6) or (exit_timestamp.date() in holidays):
            exit_holiday = True

        # Esto sirve para ajustar el workday del sabado automaticamente
        # y no se paguen extras si no ha completado las horas de trabajo
        # obligatorias a la semana
        """
        FIXME
        if enter_timestamp.weekday() == 5:
            monday = enter_timestamp.date() - datetime.timedelta(5)
            friday = enter_timestamp.date() - datetime.timedelta(1)
            monday_dts = datetime.datetime(monday.year, monday.month, monday.day)
            friday_dts = datetime.datetime(friday.year, friday.month, friday.day, 23, 59, 59)
            accesses = self.search([
                    ('employee', '=', employee.id),
                    ('enter_timestamp', '>=', monday_dts),
                    ('enter_timestamp', '<=', friday_dts),
                    ])
            total_work_week = sum([acc.ttt for acc in accesses])
            # Nuevo dia de trabajo solo para los sabados
            if not config.week_hours_work:
                self.raise_user_error('missing_week_hours_work',)
            workday = config.week_hours_work - total_work_week
        """

        # To convert datetime enter/exit to decimal object
        enterd = self._datetime2decimal(enter_timestamp)
        exitd = self._datetime2decimal(exit_timestamp)

        if start_rest and end_rest:
            start_rest_timestamp = start_rest.replace(tzinfo=from_zone)
            start_rest_timestamp = start_rest_timestamp.astimezone(to_zone)
            end_rest_timestamp = end_rest.replace(tzinfo=from_zone)
            end_rest_timestamp = end_rest_timestamp.astimezone(to_zone)

            start_rest = self._datetime2decimal(start_rest_timestamp)
            end_rest = self._datetime2decimal(end_rest_timestamp)

        # To check whether date change inside of shift
        date_change = enter_timestamp.date() != exit_timestamp.date()

        liquid = self._calculate_shift(
            enterd, exitd, date_change, enter_holiday, exit_holiday,
            workday, restday, start_rest, end_rest, restday_effective,
        )

        res = {
            'ttt': liquid['ttt'],
            'het': liquid['het'],
            'reco': liquid['reco'],
            'recf': liquid['recf'],
            'dom': liquid['dom'],
            'hedo': liquid['hedo'],
            'heno': liquid['heno'],
            'hedf': liquid['hedf'],
            'henf': liquid['henf'],
        }
        return res

    def _calculate_shift(
        self, enterd, exitd, date_change, enter_holiday, exit_holiday, workday,
            restday, start_rest, end_rest, restday_effective):
        ttt = het = hedo = heno = reco = recf = dom = hedf = henf = _ZERO

        if date_change:
            exitd += 24

        # T.T.T.
        ttt = exitd - enterd - restday_effective
        if ttt <= 0:
            ttt = 0
            return {'ttt': ttt, 'het': het, 'hedo': hedo, 'heno': heno,
                'reco': reco, 'recf': recf, 'dom': dom, 'hedf': hedf, 'henf': henf}

        # H.E.T.
        workday_legal = Decimal(workday - restday)
        # workday_effective = workday - restday_effective?
        if ttt > workday_legal:
            het = ttt - workday_legal

        contador = enterd  # Contador que comienza con la hora de entrada
        total = 0  # Sumador que comienza en Cero
        in_extras = False
        cicle = True
        rest_moment = False
        # ---------------------- main iter -----------------------------
        while cicle:
            # Ciclo Inicial
            if contador == enterd:
                if int(enterd) == int(exitd):
                    # Significa que entro y salio en la misma hora
                    sumador = exitd - contador
                    cicle = False
                else:
                    # Significa que salio en una hora distinta a la que entro
                    # Si entra en una hora en punto, suma una hora de lo contratrio suma el parcial de la hora
                    sumador = 1 if int(enterd) == enterd else int(enterd) + 1 - enterd
            elif contador >= int(exitd):
                # Ciclo Final
                sumador = exitd - int(exitd)
                cicle = False
            else:
                # Ciclo Intermedio
                sumador = 1

            contador = contador + sumador

            if start_rest and end_rest:
                if contador == start_rest:
                    pass
                elif (int(contador) - 1) == int(start_rest) and not rest_moment:
                    # Ajusta sumador por empezar descanso
                    rest_moment = True
                    sumador = start_rest - (contador - 1)
                elif contador >= start_rest and contador <= end_rest:
                    continue
                elif (int(contador) - 1) == int(end_rest) and rest_moment:
                    # Ajusta sumador por terminar descanso
                    sumador = contador - end_rest

            total = total + sumador
            is_night = True
            if (6 < contador <= 21) or (30 < contador <= 46):
                is_night = False

            # Verifica si hay EXTRAS
            sum_partial_rec = 0

            if total > workday:
                # Se calcula el sumador para extras
                in_extras = True
                sum_extra = sumador
                if (total - sumador - restday) <= workday_legal:
                    sum_extra = (total - restday) - workday_legal
                    sum_partial_rec = sumador - sum_extra

                if (contador <= 24 and not enter_holiday) or \
                    (contador > 24 and not exit_holiday):
                    if is_night:
                        heno = self._get_sum(heno, sum_extra)
                    else:
                        hedo = self._get_sum(hedo, sum_extra)
                else:
                    if is_night:
                        henf = self._get_sum(henf, sum_extra)
                    else:
                        hedf = self._get_sum(hedf, sum_extra)

            # Verifica si hay DOM
            if not in_extras:
                if (enter_holiday and contador <= 24) or (exit_holiday and contador > 24):
                    dom = self._get_sum(dom, sumador)
                # if dom >= WORKDAY_DEFAULT:
                #     dom = WORKDAY_DEFAULT
                if dom >= workday_legal:
                    dom = workday_legal
            # Verifica si hay REC
            if sum_partial_rec > 0:
                in_extras = False
                sum_rec = sum_partial_rec
            else:
                sum_rec = sumador

            if is_night and not in_extras:
                if (contador <= 24 and not enter_holiday) or \
                    (contador > 24 and not exit_holiday):
                    reco = self._get_sum(reco, sum_rec)
                else:
                    recf = self._get_sum(recf, sum_rec)
            if not in_extras and sum_partial_rec:
                if (enter_holiday and contador <= 24) or (exit_holiday and contador > 24):
                    dom = self._get_sum(dom, sum_partial_rec)

        return {
            'ttt': round_value(ttt), 'het': round_value(het),
            'hedo': round_value(hedo), 'heno': round_value(heno),
            'reco': round_value(reco), 'recf': round_value(recf),
            'dom': round_value(dom), 'hedf': round_value(hedf),
            'henf': round_value(henf),
        }

    def _get_sum(self, val, sumator):
        return (float(val) + float(sumator))

    def _datetime2decimal(self, data):
        res = float(data.minute) / 60 + data.hour
        res = str(round(res, 2))
        return Decimal(res)

    def timedelta2decimal(self, data):
        days2seconds = data.days * 24 * 60 * 60
        res = float(data.seconds + days2seconds) / 3600
        res = str(round(res, 2))
        return Decimal(res)

    def compute_shift_amount_add(self):
        res = 0
        if self.enter_timestamp and self.exit_timestamp:
            pass
        self.shift_amount_add = res
        self.save()


class AccessDailyStart(ModelView):
    "Access Daily Start"
    __name__ = 'staff_access.daily.start'
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    #project = fields.Many2One('project.work', 'Project')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class AccessDaily(Wizard):
    "Access Daily"
    __name__ = 'staff_access.daily'
    start = StateView('staff_access.daily.start',
        'staff_access_extratime.daily_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('staff_access.daily_report')

    def do_print_(self, action):
        pool = Pool()
        Access = pool.get('staff.access')

        start_time = time(0, 0, 0)
        end_time = time(23, 59, 0)

        start = datetime.combine(self.start.start_date, start_time)
        end = datetime.combine(self.start.end_date, end_time)
        start = start + timedelta(hours=5)
        end = end + timedelta(hours=5)

        dom = [
            ('enter_timestamp', '>=', start),
            ('enter_timestamp', '<=', end),
        ]
        if self.start.project:
            dom.append(
                ('project', '=', self.start.project.id),
            )
        access = Access.search(dom)
        data = {
            'ids': [ac.id for ac in access],
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            # 'project': None,
            # 'project_name': '',
        }

        # if self.start.project:
        #     data.update({
        #         'project': self.start.project.id,
        #         'project_name': self.start.project.name,
        #     })
        return action, data

    def transition_print_(self):
        return 'end'


class AccessDailyReport(Report):
    __name__ = 'staff_access.daily_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        user = pool.get('res.user')(Transaction().user)
        report_context['start_date'] = data['start_date']
        report_context['company'] = user.company
        report_context['records'] = records
        report_context['project'] = data.get('project_name', '')
        return report_context


class ValidateAccess(Wizard):
    "Validate Access"
    __name__ = 'staff_access.validate_access'
    start_state = 'validate_access'
    validate_access = StateTransition()

    def transition_validate_access(self):
        Access = Pool().get('staff.access')
        ids = Transaction().context['active_ids']
        if not ids:
            return 'end'
        access = Access.browse(ids)
        for acc in access:
            if acc.state != 'close':
                continue
            Access.write([acc], {'enter_timestamp': acc.enter_timestamp})

        return 'end'
