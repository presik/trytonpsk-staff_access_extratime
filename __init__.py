# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool

from . import employee
from . import access
from . import payroll


def register():
    Pool.register(
        access.StaffAccess,
        access.AccessDailyStart,
        payroll.Payroll,
        payroll.PayrollGroupStart,
        employee.Employee,
        module='staff_access_extratime', type_='model')
    Pool.register(
        access.AccessDailyReport,
        module='staff_access_extratime', type_='report')
    Pool.register(
        access.ValidateAccess,
        access.AccessDaily,
        payroll.PayrollGroup,
        module='staff_access_extratime', type_='wizard')
